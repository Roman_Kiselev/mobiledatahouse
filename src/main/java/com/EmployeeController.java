package com;


import com.dao.EmployeeDao;
import com.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class EmployeeController {

    private final EmployeeDao employeeDao;

    @Autowired
    public EmployeeController(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public String saveOrUpdateUser(@ModelAttribute("employeeForm")  Employee employee) {
        employeeDao.addEmployee(employee);
        return "redirect:/";
    }

}
