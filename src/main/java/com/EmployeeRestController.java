package com;

import com.dao.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/employee", produces = "application/json; charset=UTF-8")
public class EmployeeRestController {
    private final EmployeeDao employeeDao;

    @Autowired
    public EmployeeRestController(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Object getEmployeeById(@PathVariable Long id){
        return employeeDao.getEmployeeById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Object getEmployee(){
        return employeeDao.getEmployee();
    }
}
