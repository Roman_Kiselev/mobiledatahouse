package com;

import com.model.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {
    @RequestMapping("/")
    public String welcome(Model model) {
        Employee employee = new Employee();
        model.addAttribute("employeeForm", employee);
        return "welcome";
    }
}