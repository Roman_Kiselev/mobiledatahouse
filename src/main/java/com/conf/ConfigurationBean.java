package com.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
@ComponentScan(value = {"com"})
public class ConfigurationBean {

    @Value("classpath:seed-data.sql")
    private Resource H2_SCHEMA_SCRIPT;

    @Value("classpath:test-data.sql")
    private Resource H2_DATA_SCRIPT;


    @Bean
    public DataSource dataSource(){
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        return builder.setType(EmbeddedDatabaseType.H2)
                .addScript("seed-data.sql")
                .addScript("test-data.sql")
                .build();
    }
}
