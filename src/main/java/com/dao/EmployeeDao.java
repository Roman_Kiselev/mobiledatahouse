package com.dao;

import com.model.Employee;

import java.util.List;

public interface EmployeeDao {
    void addEmployee(Employee employee);
    List<Employee> getEmployee();
    Employee getEmployeeById(Long id);
}
