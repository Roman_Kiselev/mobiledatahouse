package com.dao.impl;

import com.dao.EmployeeDao;
import com.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    private final DataSource dataSource;
    private List<Employee>list = new ArrayList<>();

    @Autowired
    public EmployeeDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addEmployee(Employee employee) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "insert into \"employee\" (fio, department, \"position\") values(?,?,?)";
        template.update(sql, employee.getFio(), employee.getDepartment(), employee.getPosition());
    }

    @Override
    public List<Employee> getEmployee() {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        String sql = "select * from \"employee\"";
        return template.query(sql, (resultSet, i) -> new Employee(resultSet.getLong("id"),
                resultSet.getString("fio"), resultSet.getString("department"),
                resultSet.getString("position")));
    }

    @Override
    public Employee getEmployeeById(Long id) {
        String sql = "select * from \"employee\" where id = ?";
        try(PreparedStatement stmt = dataSource.getConnection().prepareStatement(sql)){
            stmt.setLong(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()){
                    return new Employee(
                            rs.getLong("id"),
                            rs.getString("fio"),
                            rs.getString("department"),
                            rs.getString("position"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
