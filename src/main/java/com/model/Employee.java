package com.model;

public class Employee {
    private Long id;
    private String fio;
    private String department;
    private String position;

    public Employee() {
    }

    public Employee(Long id, String fio, String department, String position) {
        this.id = id;
        this.fio = fio;
        this.department = department;
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
