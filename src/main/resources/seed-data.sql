create sequence employee_seq;

CREATE TABLE "employee"
(
  id bigint default employee_seq.nextval primary key,
  fio text,
  department text,
  "position" text
);

