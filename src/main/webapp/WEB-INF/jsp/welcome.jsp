<!DOCTYPE html>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html lang="en">
<head>

</head>
<body>



<div class="container">

    <form:form method="post" modelAttribute="employeeForm" action="${pageContext.request.contextPath}/employee">
        <table>
            <tr><td>
                ФИО </td><td><form:input path="fio" type="text" /> </td></tr>
            <tr><td>Отдел</td><td> <form:input path="department" type="text" /> </td></tr>
            <tr><td>Должность</td><td> <form:input path="position" type="text" /> </td></tr>
            </table>
        <button type="submit" >Сохранить
        </button>

    </form:form>

</div>


</body>

</html>